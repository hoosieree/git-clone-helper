# git-clone-helper
Makes it a little easier to manage multiple ssh keys, for working with github/gitlab/multiple accounts, etc.

The program will try to git-clone the provided repo based on ssh identities from `~/.ssh/config`.
If there are multiple matches (or host doesn't match any identities) it will prompt the user to resolve the ambiguity.

The `-k` flag (plus arguments) runs `ssh-keygen` and updates the `~/.ssh/config` file with the new entry.

## Usage
It's like some kind of... *wizard*!

    $ ls  # nothing here
    $ clone git@gitlab.com:hoosieree/git-clone-helper.git  # pasted from gitlab's "copy to clipboard" button
    Which SSH alias?
    [1] hoosieree_Bitbucket.pub
    [2] hoosieree_gitlab.pub
    [3] id_rsa.pub
    2
    Cloning into 'git-clone-helper'...
    remote: Counting objects: 13, done.
    remote: Compressing objects: 100% (11/11), done.
    remote: Total 13 (delta 1), reused 0 (delta 0)
    Receiving objects: 100% (13/13), done.
    Resolving deltas: 100% (1/1), done.
    Checking connectivity... done.
    $ ls  # repo has been downloaded
    git-clone-helper

## Prerequesites
1. `git`
2. `J` version 706
3. `make`
4. `sudo`

## Install

    $ cd git-clone-helper/j ; make install
