#!/usr/bin/env j
help =: (0 :0) rplc 'NAME';'/'taketo&.|.1{::ARGV
Usage:
  NAME repo [ -n newname ]
  NAME -l
  NAME -h

Examples:

  NAME git@example.com:user/repo.git
    Clones `repo` into current directory.
    Infers ssh key. (prompts user to decide if inference fails)

  NAME git@example.com:user/repo.git -n foo
    As above, but rename the repo to `foo`.

  NAME https://example.com/user/repo.git
    Warn, and defer to git clone of repo over https instead.

  NAME -l
    Display table of ssh entries.

  NAME -h
    Show this help message.

)

args =: 2}.ARGV
err =: 3 :0
  if. $y do. echo 'error';y end.
  exit echo help
)

NB. ~/.ssh/config entries are delimited by 'Host ...' and each line contains only 1 (k:v) pair.
nocomment =: ('#.*';'')rxrplc fread'~/.ssh/config'
id =: ,.(cut&>;.1~('Host '-:5{.])&>)cutLF nocomment
list =: (([:~.{."1),{:"1)_2]\"1 id

NB. another attempt at same (but handle kv pairs better)
sshconfig1 =: a:cut(cut;._2~LF=]) nocomment  NB. cut on LF2

hostname =: '@'takeafter':'taketo]

infer =: 3 :0
  repo =: >{.y
  name =: ' '"_`(' ',>@{:)@.(2=#) y
  if. 'https:' -: 6{.repo do.
     echo repo
     echo 'WARNING: cloning with https instead of git.  Good luck!'
     exit shell 'git clone ', repo, name
  end.
  host =: hostname repo
  choice =. list #~+./"1 list=<host
  if. 1=#choice do.
    exit shell 'git clone ', (repo rplc host;(0{::choice)), name
  else.
    donext 'interactive repo;host;name'
  end.
)

NB. Implements a simple REPL-in-a-program
NB. from http://stackoverflow.com/a/26976590/2037637  (thanks Tangentstorm)
readln =: [: (1!:1) 1:
donext =: [: (9!:29) 1: [ 9!:27

interactive =: 3 :0 :: err
  'repo host dest' =. y
  keys =. {."1 }.list
  echo (<'Which ssh alias?'),<(;~"0[:i.#) keys
  choice =. keys {::~ ".readln''
  echo str =. (repo rplc host;choice),dest
  exit shell 'git clone ',str
)

NB. call help and exit
3 :'if. y do. err '''' end.' (<'-h') e. args

NB. main program; dispatch based on command-line arguments
(3 :0 :: err) args
  if. 0=$y do. err'' end.
  if. y e.~<'debug' do. return. end.
  if. y e.~<'-l' do. exit echo list end.
  infer y-.<'-n'
)

