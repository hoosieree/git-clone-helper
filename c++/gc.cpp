/** git-clone-helper
    Simplify cloning git repos when you have multiple ssh keys.
    https://gitlab.com/hoosieree/git-clone-helper */
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <glob.h>
using namespace std;

void show_help(void){
  cout << "usage:" << endl;
  cout << "  clone <repo> [optional-new-name-for-repo]" << endl;
  cout << "example:" << endl;
  cout << "  clone git@host.com/username/reponame.git" << endl;
}


/*
  The git ssh url is of the form
  git@gitlab.com:hoosieree/git-clone-helper.git
  Our ~/.ssh/config file has an entry for github.com/hoosieree, pointing to the public key shared with github.com:
*/

int main(int argc, char *argv[]){
  if(argc<2){show_help(); return EXIT_FAILURE;}
  string name{argv[1]};
  string dest{argc==3?argv[2]:""}; // destination path for cloned repo
  string gita{name.substr(0,4)};
  string repo{name.substr(name.find(':')+1)}; // everything after colon

  cout << "Which SSH alias?\n";

  /* list all ssh aliases */
  glob_t glob_result;
  glob("~/.ssh/*.pub", GLOB_TILDE, NULL, &glob_result);

  /* put them in a vector */
  vector<string>pubs;
  for(int i=0;i<glob_result.gl_pathc;++i){
    string res{glob_result.gl_pathv[i]};
    res = res.substr(res.rfind('/')+1); // trim
    cout << "[" << i << "] " << res << "\n"; // display
    res = res.substr(0, res.find('.')); // trim some more
    pubs.push_back(res); // save for later
  }
  globfree(&glob_result);

  char c;
  if(cin >> c){
    int idx{static_cast<int>(c-'0')};
    if(idx<0 || idx>=pubs.size()){return EXIT_FAILURE;}

    /* clone the repo. */
    string result{"git clone " + gita + pubs[idx] + ":" + repo + " " + dest};
    system(result.c_str());
  }
  return EXIT_SUCCESS;
}
